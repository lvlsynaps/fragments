package com.example.sbt_voronezhtsev_in.fragmentssample;

import android.widget.EditText;

/**
 * Created by sbt-voronezhtsev-in on 18.06.2018.
 */

public interface ActivityCallBack {
    EditText getText();
}
