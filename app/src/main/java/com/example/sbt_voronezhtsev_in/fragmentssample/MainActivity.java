package com.example.sbt_voronezhtsev_in.fragmentssample;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements ActivityCallBack{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, Service.class);
        startService(intent);
    }

    @Override
    public EditText getText() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.first_fragment);
        if(!(fragment instanceof FirstFragment)) {
            throw new ClassCastException("Фрагмент 1 не экземпляр класса FirstFragment");
        }
        FirstFragment firstFragment  = (FirstFragment) getSupportFragmentManager().findFragmentById(R.id.first_fragment);
        return firstFragment.getEditText();
    }
}
