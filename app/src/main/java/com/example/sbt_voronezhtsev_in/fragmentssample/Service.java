package com.example.sbt_voronezhtsev_in.fragmentssample;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by sbt-voronezhtsev-in on 18.06.2018.
 */

public class Service extends IntentService {
    private  int mData;
    Thread thread;
    public Service() {

        super("service");
    }
    @Override
    protected void onHandleIntent(@Nullable final Intent intent) {

            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {
                        mData++;
                        Intent intent = new Intent("ru.voronezhtsev.Fragments");
                        intent.putExtra("textData", String.valueOf(mData));
                        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        sendBroadcast(intent);
                        try {
                            Thread.sleep(1000l);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            });

            thread.start();
        }

}
